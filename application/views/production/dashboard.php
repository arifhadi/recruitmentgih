<div class="page">
  <?php if ($this->session->flashdata('success')) { ?>
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button><p><?php echo $this->session->flashdata('success'); ?></p>
    </div>
  <?php }elseif($this->session->flashdata('error')){ ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button><p><?php echo $this->session->flashdata('error'); ?></p>
    </div>
  <?php } ?>
  
  <div class="page-header" style="text-align: center; padding: 0px;">
    <br>
   <h1 class="page-title gradient-text">Recruitment PT.Galaksi Investasi Harapan</h1>
  </div>
  <br>
  <div class="page-content" style="padding: 0px;">
    <div class="panel">
      <div class="panel-body container-fluid" style="padding: 0px;">
        <div class="panel">
          <div class="panel-body container-fluid">
            <div class="row row-lg">
              <div class="col-md-12 col-lg-6">
                <div class="example-wrap">
                  <div class="example">
                    <!-- <form class="form-horizontal"> -->
                      <form id="uploadForm">
                      <div class="form-group row">
                        <label class="col-md-4 form-control-label"><b>Nama Lengkap (Sesuai KTP)<b style="color: red;">*</b>: </b></label>
                        <div class="col-md-8">
                            <input type="text" required="required" autofocus="autofocus" id="full_name" class="form-control" name="full_name" placeholder="Nama Lengkap" autocomplete="off" />
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-4 form-control-label"><b>Email<b style="color: red;">*</b>  </b></label>
                        <div class="col-md-8">
                         <input type="email" required="required" class="form-control" name="email" placeholder="Email" autocomplete="off" id="email" />
                       </div>
                     </div>
                    <div class="form-group row">
                      <label class="col-md-4 form-control-label"><b>Posisi Yang Dilamar<b style="color: red;">*</b>  </b></label>
                      <div class="col-md-8">
                       <select class="form-control" required="required" data-plugin="select2" id="position_id" name="position_id" data-placeholder="Pilih Posisi">
                          <option></option>
                            <?php foreach ($list_position as $val) { ?>
                              <option value="<?php echo $val->id?>">
                                <?php echo "$val->name_position" ?>
                              </option>
                            <?php } ?>                        
                          </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-md-4 form-control-label"><b>No.KTP<b style="color: red;">*</b></b></label>
                      <div class="col-md-8">
                        <input type="number" required="required" class="form-control" name="ktp" placeholder="KTP" autocomplete="off" id="ktp" />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-md-4 form-control-label"><b>No.NPWP<b style="color: red;">*</b></b></label>
                      <div class="col-md-8">
                        <input type="number" required="required" class="form-control" name="npwp" placeholder="NPWP" autocomplete="off" id="npwp" />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-md-4 form-control-label"><b>Alamat Tempat Tinggal<b style="color: red;">*</b></b></label>
                      <div class="col-md-8">
                        <textarea class="maxlength-textarea form-control" data-plugin="maxlength" data-placement="bottom-right-inside" maxlength="200" name="address" rows="3" id="address" placeholder="Alamat Tempat Tinggal"></textarea>
                      </div>
                    </div>

                   <div class="form-group row">
                      <label class="col-md-4 form-control-label"><b>Kontak Darurat (isikan dengan nomor handphone)<b style="color: red;">*</b></b></label>
                      <div class="col-md-8">
                        <input type="number" required="required" class="form-control" name="emergency_contact" placeholder="Kontak Darurat (isikan dengan nomor handphone)" autocomplete="off" id="emergency_contact" />
                      </div>
                    </div>

                     <div class="form-group row">
                      <label class="col-md-4 form-control-label"><b>No Handphone<b style="color: red;">*</b></b></label>
                      <div class="col-md-8">
                        <input type="number" required="required" class="form-control" name="phone_number" placeholder="No Handphone" autocomplete="off" id="phone_number" />
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-4 form-control-label"><b>Jenis Kelamin<b style="color: red;">*</b></b></label>
                      <div class="col-md-8">
                         <input type="radio" class="status_half_day" value="1" name="gender" id="Active1" required >&ensp;<label class="badge badge-success" style="font-size: 12px;" for="Active1" dreq>&emsp;Laki Laki&emsp;</label>&emsp;
                        <input type="radio" class="status_half_day" value="0" name="gender" id="Nonactive1">&ensp;<label class="badge badge-danger" style="font-size: 12px;" for="Nonactive1">Perempuan</label>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-md-4 form-control-label"><b>Golongan Darah<b style="color: red;">*</b></b></label>
                      <div class="col-md-8">
                        <input type="text" required="required" class="form-control" name="blood_type" placeholder="Golongan Darah" autocomplete="off" id="blood_type" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="col-md-12 col-lg-6">
                <div class="example-wrap">
                  <div class="example">
                    <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Tempat Lahir<b style="color: red;">*</b></b></label>
                      <div class="col-md-9">
                        <input type="text" required="required" class="form-control" name="place_birth" placeholder="Tempat Lahir" autocomplete="off" id="place_birth" />
                      </div>
                    </div>

                  <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Tanggal Lahir<b style="color: red;">*</b></b></label>
                      <div class="col-md-9">
                        <input type="date" required="required" class="form-control" name="date_birth" placeholder="Tanggal Lahir" autocomplete="off" id="date_birth" />
                      </div>
                    </div>

             <div class="form-group row">
              <label class="col-md-3 form-control-label"><b>Status Pernikahan<b style="color: red;">*</b> : </b></label>
              <div class="col-md-9">
                <select class="form-control" required data-plugin="select2" id="status_married" name="status_married" data-placeholder="Pilih Status Pernikahan">
                  <option></option>
                  <option>Menikah</option>
                  <option>Belum Menikah</option>
                  <option>Cerai</option>
                </select>
            </div>
          </div>
                <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Agama<b style="color: red;">*</b>  </b></label>
                      <div class="col-md-9">
                       <select class="form-control" required data-plugin="select2" id="religion" name="religion" data-placeholder="Pilih Agama">
                          <option></option>
                          <option>Islam</option>
                          <option>Kristen</option>
                          <option>Buddha</option>
                          <option>Hindu</option>
                        </select>
                      </div>
                 </div>
              <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Pendidikan Terakhir<b style="color: red;">*</b>  </b></label>
                      <div class="col-md-9">
                       <select class="form-control" required data-plugin="select2" id="last_education" name="last_education" data-placeholder="Pilih Pendidikan Terakhir">
                          <option></option>
                          <option>SMA</option>
                          <option>SMK</option>
                          <option>D3</option>
                          <option>D4</option>
                          <option>S1</option>
                          <option>S2</option>
                          <option>S3</option>
                        </select>
                      </div>
                 </div>
                <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Nama Sekolah/Universitas<b style="color: red;">*</b></b></label>
                      <div class="col-md-9">
                        <input type="text" required="required" class="form-control" name="name_school" placeholder="Nama Sekolah/Universitas" autocomplete="off" id="name_school" />
                      </div>
                </div>

              <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Jurusan<b style="color: red;">*</b></b></label>
                      <div class="col-md-9">
                        <input type="text" required="required" class="form-control" name="field_education" placeholder="Jurusan" autocomplete="off" id="field_education" />
                      </div>
                </div>
              <div class="form-group row">
                      <label class="col-md-3 form-control-label"><b>Tahun Lulus<b style="color: red;">*</b></b></label>
                      <div class="col-md-9">
                        <input type="number" required="required" class="form-control" name="graduation_year" placeholder="Tahun Lulus" autocomplete="off" id="graduation_year" />
                      </div>
                </div>



                  <div class="form-group row">
                         <label class="col-md-3 form-control-label"><b>Sertifikat Vaksin Dosis 3<b style="color: red;">*</b></b></label>
                        <div class="col-md-9">
                          <div class="input-group input-group-file" data-plugin="inputGroupFile">
                            <input  type="text" class="form-control" readonly="">
                            <span class="input-group-append">
                              <span class="btn btn-success btn-file">
                                <i class="icon md-upload" aria-hidden="true"></i>
                                <input type="file" name="userfile" id="userfile" multiple="" required>
                              </span>
                            </span>
                          </div>
                        </div>
                      </div>


                    <div class="form-group row  row">
                        <label class="col-md-3 form-control-label"><b>Upload CV dan Dokumen Lainnya<b style="color: red;">*</b></b></label>
                        <div class="col-md-9">
                          <div class="input-group input-group-file" data-plugin="inputGroupFile">
                            <input type="text" class="form-control" readonly="">
                            <span class="input-group-append">
                              <span class="btn btn-success btn-file">
                                <i class="icon md-upload" aria-hidden="true"></i>
                                <input type="file" id="file_document" name="file_document" multiple="" required>
                              </span>
                            </span>
                          </div>
                        </div>
                    </div>


                    <div id="driverDiv" style="display: none;">

                      <div class="form-group row  row">
                        <label class="col-md-3 form-control-label"><b>SIM B1 umum<b style="color: red;">*</b></b></label>
                        <div class="col-md-9">
                          <div class="input-group input-group-file" data-plugin="inputGroupFile">
                            <input type="text" class="form-control" readonly="">
                            <span class="input-group-append">
                              <span class="btn btn-success btn-file">
                                <i class="icon md-upload" aria-hidden="true"></i>
                                <input type="file" id="file_sim" name="file_sim" multiple="">
                              </span>
                            </span>
                          </div>
                        </div>
                    </div>


                     <div class="form-group row  row">
                        <label class="col-md-3 form-control-label"><b>SIO Forklift<b style="color: red;">*</b></b></label>
                        <div class="col-md-9">
                          <div class="input-group input-group-file" data-plugin="inputGroupFile">
                            <input type="text" class="form-control" readonly="">
                            <span class="input-group-append">
                              <span class="btn btn-success btn-file">
                                <i class="icon md-upload" aria-hidden="true"></i>
                                <input type="file" id="file_sio" name="file_sio" multiple="">
                              </span>
                            </span>
                          </div>
                        </div>
                    </div>


                  </div>

        </div>
      </div>
    </div>
    <div class="col-lg-5 form-group form-material">

    </div>
    <div class="col-lg-5 form-group form-material">
      <button type="submit" class="btn btn-success btn-sm">&emsp;&emsp;Apply&emsp;&emsp;</button>
    </div>
    <div class="col-lg-2 form-group form-material">
    </div>
    </form>
    <!-- Button Action -->
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
<div id="loader" class="lds-dual-ring hidden overlay"></div>

<style>

.lds-dual-ring.hidden { 
display: none;
}
.lds-dual-ring {
  display: inline-block;
  justify-content: center;
  align-items: center;
  width: 80px;
  height: 80px;
}
.lds-dual-ring:after {
  content: " ";
  display: block;
  width: 64px;
  height: 64px;
  margin: 23% auto;
  border-radius: 50%;
  border: 6px solid #fff;
  border-color: #fff transparent #fff transparent;
  animation: lds-dual-ring 1.2s linear infinite;
}
@keyframes lds-dual-ring {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}


.overlay {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100vh;
    background: rgba(0,0,0,.8);
    z-index: 999;
    opacity: 1;
    transition: all 0.5s;
}

.swal2-overflow {
  overflow-x: visible;
  overflow-y: visible;
  font-family: Georgia, serif;
}
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; 
  animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

</style>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.all.min.js" integrity="sha256-Ry2q7Rf2s2TWPC2ddAg7eLmm7Am6S52743VTZRx9ENw=" crossorigin="anonymous"></script>
  <script type="text/javascript">

$('#uploadForm').submit(function(e) {
    e.preventDefault();

    var formData = new FormData(this);
    var name = document.getElementById('full_name').value;
    var email = document.getElementById('email').value;
    var position_id = document.getElementById('position_id').value;
    var ktp = document.getElementById('ktp').value;
    var npwp = document.getElementById('npwp').value;
    var address = document.getElementById('address').value;
    var emergency_contact = document.getElementById('emergency_contact').value;
    var phone_number = document.getElementById('phone_number').value;
    var blood_type = document.getElementById('blood_type').value;
    var place_birth = document.getElementById('place_birth').value;
    var date_birth = document.getElementById('date_birth').value;
    var status_married = document.getElementById('status_married').value;
    var religion = document.getElementById('religion').value;
    var last_education = document.getElementById('last_education').value;
    var name_school = document.getElementById('name_school').value;
    var field_education = document.getElementById('field_education').value;
    var graduation_year = document.getElementById('graduation_year').value;
    var userfile = document.getElementById('userfile').value;
    var file_document = document.getElementById('file_document').value;
    var file_sim = document.getElementById('file_sim').value;
    var file_sio = document.getElementById('file_sio').value;


    var data_wrong = [
    '000000000000000',
    '111111111111111',
    '222222222222222',
    '333333333333333',
    '444444444444444',
    '555555555555555',
    '666666666666666',
    '777777777777777',
    '888888888888888',
    '999999999999999'
    ];

    var filteredData = data_wrong.filter(function(element) {
        return element === npwp;
      });


      if(name == '')
      {
        swal.fire("Gagal !", "Data Nama Masih Kosong", "danger");
      }

     else if(filteredData.length > 0)
      {
        swal.fire("Gagal !", "Nomor NPWP Tidak Valid", "danger");
      }
      else if(email == '')
      {
        swal.fire("Gagal !", "Data Email Masih Kosong", "danger");
      }
      else if(position_id == '')
      {
        swal.fire("Gagal !", "Data Posisi Yang Dilamar Masih Kosong", "danger");
      }
      else if(npwp == '')
      {
        swal.fire("Gagal !", "Data NPWP Yang Dilamar Masih Kosong", "danger");
      }

      else if(npwp == '0')
      {
        swal.fire("Gagal !", "Data NPWP TidaK Boleh Berisi 0", "danger");
      }

      else if(npwp.length < 15)
      {
        swal.fire("Gagal !", "Data NPWP TidaK Boleh Berisi Kurang Dari 15 Digit", "danger");
      }

      else if(ktp == '')
      {
        swal.fire("Gagal !", "Data KTP Masih Kosong", "danger");
      }

      else if(ktp == '0')
      {
        swal.fire("Gagal !", "Data KTP Tidak Boleh Berisi 0", "danger");
      }

      else if(ktp.length < 16)
      {
        swal.fire("Gagal !", "Data KTP TidaK Boleh Berisi Kurang Dari 15 Digit", "danger");
      }

      else if(address == '')
      {
        swal.fire("Gagal !", "Data Alamat Masih Kosong", "danger");
      }

      else if(emergency_contact == '')
      {
        swal.fire("Gagal !", "Data Kontrak Darurat Masih Kosong", "danger");
      }

      else if(phone_number == '')
      {
        swal.fire("Gagal !", "Data No Handphone Masih Kosong", "danger");
      }

      else if(blood_type == '')
      {
        swal.fire("Gagal !", "Data Golongan Darah Masih Kosong", "danger");
      }

      else if(place_birth == '')
      {
        swal.fire("Gagal !", "Data Tempat Lahir Masih Kosong", "danger");
      }

      else if(date_birth == '')
      {
        swal.fire("Gagal !", "Data Tanggal Lahir Masih Kosong", "danger");
      }

      else if(status_married == '')
      {
        swal.fire("Gagal !", "Data Status Pernikahan Masih Kosong", "danger");
      }

      else if(religion == '')
      {
        swal.fire("Gagal !", "Data  Agama Masih Kosong", "danger");
      }

      else if(last_education == '')
      {
        swal.fire("Gagal !", "Data  Pendidikan Terakhir Masih Kosong", "danger");
      }

      else if(name_school == '')
      {
        swal.fire("Gagal !", "Data  Nama Sekolah Masih Kosong", "danger");
      }

      else if(field_education == '')
      {
        swal.fire("Gagal !", "Data  Jurusan Sekolah/Universitas Masih Kosong", "danger");
      }

      else if(graduation_year == '')
      {
        swal.fire("Gagal !", "Data  Tahun Lulus Masih Kosong", "danger");
      }

      else if(userfile == '')
      {
        swal.fire("Gagal !", "Data Upload Vaksin Masih Kosong", "danger");
      }

      else if(file_document == '')
      {
        swal.fire("Gagal !", "Data Upload File Document Lainnya Masih Kosong", "danger");
      }

      else{

          if(position_id == 14)
          {
              if(file_sim == '' || file_sio == '')
              {
                  swal.fire("Gagal !", "Data Upload File SIM atau SIO Masih Kosong", "danger");
              }
              else{
                Swal.fire({
                  title: "Pastikan Anda Mengisi Data Dengan Benar !",
                  type: 'question',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#66bb6a',
                  cancelButtonColor: '#ef5350',
                  confirmButtonText: 'Yes, Confirm!'
                }).then((result) => {
                  if (result.value == true) {
                    $.ajax({
                      type: 'POST',
                      enctype: 'multipart/form-data',
                      url: '<?= base_url("dashboard/apply_job")?>',
                      data: formData,
                      processData: false,
                      contentType: false,
                      beforeSend: function() {
                        $('#loader').removeClass('hidden');
                      },
                      error: function(data) {
                        swal.fire("Maaf !", data.responseJSON.text, "warning");
                        $('#loader').addClass('hidden');
                      },
                      success: function(data) {
                        if(data =="")
                        {
                          swal.fire("Berhasil Melamar Pekerjaan!", data.text, "success");
                        }
                        else{
                          swal.fire("Gagal !", "Ukuran File Lebih dari 2MB atau File Bukan Berekstensi PDF,JPEG,JPG,atau PNG", "danger");
                        }

                        setInterval(window.location.href = '<?= base_url("dashboard/finish_apply")?>', 3000);
                        $('#loader').addClass('hidden');
                      },
                      complete: function (){
                        $('#loader').addClass('hidden');
                      },
                    });

                  }else if(result.value == undefined){
                    Swal.fire(
                      'Cancelled !',
                      'Your file has been deleted.',
                      'error'
                      )
                  }
                })
              }
            }
            else{

              Swal.fire({
                title: "Pastikan Anda Mengisi Data Dengan Benar !",
                type: 'question',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#66bb6a',
                cancelButtonColor: '#ef5350',
                confirmButtonText: 'Yes, Confirm!'
              }).then((result) => {
                if (result.value == true) {
                  $.ajax({
                    type: 'POST',
                    enctype: 'multipart/form-data',
                    url: '<?= base_url("dashboard/apply_job")?>',
                    data: formData,
                    processData: false,
                    contentType: false,
                    beforeSend: function() {
                      $('#loader').removeClass('hidden');
                    },
                    error: function(data) {
                      swal.fire("Maaf !", data.responseJSON.text, "warning");
                      $('#loader').addClass('hidden');
                    },
                    success: function(data) {
                      if(data =="")
                      {
                        swal.fire("Berhasil Melamar Pekerjaan!", data.text, "success");
                      }
                      else{
                        swal.fire("Gagal !", "Ukuran File Lebih dari 2MB atau File Bukan Berekstensi PDF,JPEG,JPG,atau PNG", "danger");
                      }

                      setInterval(window.location.href = '<?= base_url("dashboard/finish_apply")?>', 3000);
                      $('#loader').addClass('hidden');
                    },
                    complete: function (){
                      $('#loader').addClass('hidden');
                    },
                  });

                }else if(result.value == undefined){
                  Swal.fire(
                    'Cancelled !',
                    'Your file has been deleted.',
                    'error'
                    )
                }
              })


            }
        }








})


function reload()
{
  window.location.reload();
}





  $(function () {
    $("#position_id").change(function() {
          var val = $(this).val();
          if(val == 14)
          {
            var divDriv = document.getElementById("driverDiv");
            divDriv.style.display = "block";
          }
          else{

            var divDriv = document.getElementById("driverDiv");
            divDriv.style.display = "none";
          }

    });
  });
  </script>
<!-- End Page