<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Model Register
 */
class M_employee extends CI_Model{

	// private $table1 = 'u_user_register';
	// private $table2 = 'users';
	// const MAX_PASSWORD_SIZE_BYTES = 4096;

	public function __construct()
	{
		date_default_timezone_set('Asia/Jakarta');
	}

	public function check_nik($nik)
	{
		$this->db->select('ktp');
		$this->db->from('m_recruitment');
		$this->db->where('ktp',$nik);
		return $this->db->get()->row();
	}

	public function insert_data_recruitment($data)
	{
		return $this->db->insert('m_recruitment', $data);
	}

	public function get_data_position()
	{
		$this->db->select('id,name_position');
		$this->db->from('m_rec_position');
		$this->db->where('visible',0);
		// log_r($this->db->get()->result());
		return $this->db->get()->result();
	}



} ?>