<div class="page">
 
  <?php if ($this->session->flashdata('success')) { ?>
  <div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button><p><?php echo $this->session->flashdata('success'); ?></p>
  </div>
  <?php }elseif($this->session->flashdata('error')){ ?>
  <div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button><p><?php echo $this->session->flashdata('error'); ?></p>
  </div>
  <?php } ?>
  <div class="page-header" style="text-align: center; padding: 0px;">
    
  </div>
  <div class="page-content" style="padding: 0px;">
    <div class="panel">
      <div class="panel-body container-fluid" style="padding: 0px;">
        <div class="panel">
          <div class="panel-body container-fluid">
             <div style="text-align: center; padding: 0px;"><h3>Selamat, Anda Sudah Berhasil Melamar Pekerjaan di PT.GIH Batam!</h3></div>
              <table width="100%">
               <tr>
                <td width="50" align="center">
                 <img src="<?php echo base_url('src/material/global/assets/images/receipt_success.png');?>" width="20%">
               </td>
              </tr>
            </table> 
              <p class="text-center">Terima kasih telah ingin bergabung menjadi calon karyawan di PT.GIH Batam, Lamaran Anda Akan Di Proses Mohon Menunggu <br> dari Pihak PT.GIH Batam , Salam Sukses Untuk Kita Semua :)
              </p>
              <?php form_close() ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  
 

