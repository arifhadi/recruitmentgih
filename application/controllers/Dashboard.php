<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct(){
		parent::__construct();
	    date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('m_employee');
	}

	public function index()
	{
		// $this->m_employee->update_status_emp();
		$this->data['list_position'] = $this->m_employee->get_data_position();
		$this->usertemp->view('production/dashboard',$this->data);
	}

	public function apply_job()
	{
		 $hariIni        = new DateTime();
		 $full_name    = $this->input->post('full_name');
		 $email    = $this->input->post('email');
		 $position_id    = $this->input->post('position_id');
		 $ktp    = $this->input->post('ktp');
		 $npwp    = $this->input->post('npwp');
		 $address    = $this->input->post('address');
		 $phone_number    = $this->input->post('phone_number');
		 $blood_type    = $this->input->post('blood_type');
		 $place_birth    = $this->input->post('place_birth');
		 $status_married    = $this->input->post('status_married');
		 $religion    = $this->input->post('religion');
		 $last_education    = $this->input->post('last_education');
		 $name_school    = $this->input->post('name_school');
		 $field_education    = $this->input->post('field_education');
		 $graduation_year    = $this->input->post('graduation_year');
		 // $file_vaccinate    = $this->input->post('file_vaccinate');
		 $emergency_contact    = $this->input->post('emergency_contact');
		 $date_birth    = $this->input->post('date_birth');
		 // $file_document    = $this->input->post('file_document');
		 $gender    = $this->input->post('gender');

		 // log_r($full_name);


     // $ip = file_get_contents('https://api.ipify.org');
     // log_r($ip);
     // $ip = file_get_contents('https://api.ipify.org');

     // log_r($ip);

     if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

     // log_r($ip);
		 $check_nik = $this->m_employee->check_nik($ktp);

		 	if(empty($check_nik))
		 	{
		 		    $config['upload_path'] = './src/assets/uploads/vaccinate/';
  		  		$config['allowed_types'] = 'jpeg|jpg|png|pdf';
  		  		$config['max_size']      = 2048;
  		  		$config['file_name']     = $_FILES['userfile']['name'];

  				$this->load->library('upload', $config);
 

  				if (!$this->upload->do_upload('userfile')){
            		$fileVaccinate =null;
        		}else{
            		$fileVaccinate = $this->upload->data('file_name');
            }


            $config1['upload_path'] = './src/assets/uploads/documents/';
  		  		$config1['allowed_types'] = 'jpeg|jpg|png|pdf';
  		  		$config1['max_size']      = 2048;
  		  		$config1['file_name']     = $_FILES['file_document']['name'];
  		  		
            	$this->upload->initialize($config1);

            	if (!$this->upload->do_upload('file_document')){
            		$fileDocument =null;
        		}else{
            		$fileDocument = $this->upload->data('file_name');
            	}


              $config2['upload_path'] = './src/assets/uploads/sim/';
              $config2['allowed_types'] = 'jpeg|jpg|png|pdf';
              $config2['max_size']      = 2048;
              $config2['file_name']     = $_FILES['file_sim']['name'];
            
              $this->upload->initialize($config2);

              if (!$this->upload->do_upload('file_sim')){
                  $fileSim =null;
              }else{
                  $fileSim = $this->upload->data('file_name');
              }


              $config3['upload_path'] = './src/assets/uploads/sio/';
              $config3['allowed_types'] = 'jpeg|jpg|png|pdf';
              $config3['max_size']      = 2048;
              $config3['file_name']     = $_FILES['file_sio']['name'];
            
              $this->upload->initialize($config3);

              if (!$this->upload->do_upload('file_sio')){
                  $fileSio =null;
              }else{
                  $fileSio = $this->upload->data('file_name');
              }



  			$data = array(
  				'full_name'         => $full_name,
  				'position_id' =>$position_id,
  				'email' =>$email,
  				'ktp' =>$ktp,
  				'npwp' =>$npwp,
  				'address' =>$address,
  				'phone_number'   => $phone_number,
  				'emergency_contact'	=> $emergency_contact,
  				'gender'               => $gender,
  				'blood_type'               => $blood_type,
  				'place_birth'            => $place_birth,
  				'date_birth'   => $date_birth,
  				'last_educaction'	=> $last_education,
  				'name_school'               => $name_school,
  				'field_education'               => $field_education,
  				'graduation_year'            => $graduation_year,
  				'file_vaccinate'	=> $fileVaccinate,
  				'file_document'		=> $fileDocument,
          'file_sio'        => $fileSio,
          'file_sim'        => $fileSim,
  				'religion' => $religion,
  				'status_married' => $status_married,
  				'create_at'             => $hariIni->format('y-m-d H:i:s'),
          'ip_address' => $ip
  			);
  			$status = $this->m_employee->insert_data_recruitment($data);
		 }
		 else{
		 		return $this->output
                ->set_content_type('application/json')
                ->set_status_header(500)
                ->set_output(json_encode([
                'text' => 'Gagal , Karena Anda Sudah Pernah Melamar',
                 'type' => 'danger'
                 ]));
		 }



	}

	public function finish_apply()
	{
		$this->usertemp->view('production/finish_recruitment');
	}
	
}
